﻿using System;
using System.Drawing;

namespace PrimeFactorsTask
{
    public static class PrimeFactors
    {
        public static int[] GetFactors(int number)
        {
            if (number <= 0)
            {
                throw new ArgumentException("mistake");
            }

            int div = 2;
            int count = 0;
            int n = number;
            int[] buf = new int[7];

            while (n > 1)
            {
                while (n % div == 0)
                {
                    buf[count] = div;
                    count++;
                    n /= div;
                }

                div++;
            }

            int[] result = new int[count];
            while (count > 0)
            {
                result[count - 1] = buf[count - 1];
                count--;
            }

            return result;
        }
    }
}
